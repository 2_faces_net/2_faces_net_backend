module.exports = {
    /** Server config **/
    SERVER_PORT:            9999,
    HOSTNAME:               process.env.HOSTNAME || 'localhost',
    /** Socket config **/
    SOCKET_MAIN_PORT:       60001,
    SOCKET_CODE_SENDER:     60100,
    SOCKET_CODE_SENDER1:    60300,
    SOCKET_COLLECTOR:       60400,
    SOCKET_COLLECTOR1:      60500,
    /** Mongo config **/
    MONGO_USERNAME:         process.env.MONGO_USERNAME || '2facesnet_user',
    MONGO_PASSWORD:         process.env.MONGO_PASSWORD || '2facesnet_pass',
    MONGO_HOST:             process.env.MONGO_HOST || 'localhost',
    MONGO_PORT:             process.env.MONGO_PORT || 27017,
    MONGO_DATABASE:         process.env.MONGO_DATABASE || '2facesnet_db',
}