const express = require('express');
const environment = require('../environment')

const webRouter = express.Router();


webRouter.get('/', function (req, res) {
    const arg = environment.HOSTNAME + ":" + environment.SOCKET_MAIN_PORT;
    res.render('index', {ip_port: arg});
});


module.exports = webRouter;