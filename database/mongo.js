const Mongoose = require('mongoose');
const environment = require('./../environment')


let connectionString = 'mongodb://' +
    environment.MONGO_USERNAME + ':' +
    environment.MONGO_PASSWORD + '@' +
    environment.MONGO_HOST + ':' +
    environment.MONGO_PORT + '/' +
    environment.MONGO_DATABASE;
const mongoOptions = {useNewUrlParser: true, useUnifiedTopology: true};
console.log(connectionString)
/**
 * DB Connection
 */
Mongoose.connect(connectionString, mongoOptions);

/**
 * DB Connection error handling
 */
Mongoose.connection.on('error', (err) => {
    if (err) {
        throw err;
    }
});
Mongoose.Promise = global.Promise;

/**
 * Export of DB Connection and collections schemas
 */
module.exports = {
    Mongoose, models: {
        payload: require('./schemas/payload'),
        attackResult: require('./schemas/attackResult')
    }
};
