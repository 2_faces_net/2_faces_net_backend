FROM node:14.18.2

# Move into backend folder
WORKDIR /app

# Copy dependencies json
COPY package.json .
COPY package-lock.json .

# Install node dependencies
RUN npm i && npm audit fix --force

# Copy backend source
COPY . .

# Entry point
CMD ["npm", "run", "start"]
