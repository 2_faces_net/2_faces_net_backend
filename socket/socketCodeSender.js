const CryptedSocket = require('./CryptedSocket');
const cryptoManager = require('../utils/cryptoUtil');
const environment = require('../environment')


let socketsCodeSenderPool = new Map();


function requireFreeCodeSenderPort() {
    let port = 0;
    let poolObject;
    let timestamp = Math.floor(new Date().getTime() / 1000)
    do {
        port = cryptoManager.getRandomInteger(environment.SOCKET_CODE_SENDER, environment.SOCKET_CODE_SENDER1);
        poolObject = socketsCodeSenderPool.get(port);
    } while (poolObject && poolObject.status !== "in_use" && (timestamp - poolObject.endTime) < 1000000000);
    socketsCodeSenderPool.set(port, {status: "in_use"})
    return port;
}

async function openNewSocketCodeSender(codeSenderPort, code, numOfWrite) {
    let socketCodeSender = new CryptedSocket("SocketCodeSender", environment.HOSTNAME, codeSenderPort,
        async function onConnect(socket) {
            const stringEscaped = code.toString().replace(/(\r\n|\n|\r|\t)/gm, '');
            const stringEscapedWellTrimmed = stringEscaped.replace(/ +(?= )/g, '');
            socketCodeSender.write(socket, stringEscapedWellTrimmed);
            numOfWrite--;

            if (numOfWrite === 0) {
                socket.end();
            }
        },
        function onError(socket) {
            socket.end();
            releasePorts([codeSenderPort]);
        },
        function onClose(socket) {
            socket.end();
            releasePorts([codeSenderPort]);
        },
        function onTimeout(socket) {
            socket.end();
            releasePorts([codeSenderPort]);
        });
}

async function releasePorts(ports) {
    ports.forEach((port) => {
        socketsCodeSenderPool.set(port, {status: "not_used", endTime: Math.floor(new Date().getTime() / 1000)})
    });
}


module.exports = {
    requireFreeCodeSenderPort,
    openNewSocketCodeSender,
    releasePorts
};
