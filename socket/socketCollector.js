const net = require('net');
const cryptoManager = require('../utils/cryptoUtil');
const environment = require('../environment')


let socketsCollectorPool = new Map();


function requireFreeCodeCollectorPort() {
    let port = 0;
    let poolObject;
    let timestamp = Math.floor(new Date().getTime() / 1000)
    do {
        port = cryptoManager.getRandomInteger(environment.SOCKET_COLLECTOR, environment.SOCKET_COLLECTOR1);
        poolObject = socketsCollectorPool.get(port);
        console.log(port, poolObject, timestamp);
    } while (poolObject && poolObject.status !== "in_use" && (timestamp - poolObject.endTime) < 1000000000);
    socketsCollectorPool.set(port, {status: "in_use"})
    return port;
}

async function openSocketCollectorAndWaitForResult(collectorPort) {
    return new Promise((resolve, reject) => {
        net.createServer((socketCollector) => {
            console.log('CONNECTED_COLLECTOR: ' + socketCollector.remoteAddress + ':' + socketCollector.remotePort);

            let result = "";


            socketCollector.on('data', function (data) {
                result += data;
            });


            socketCollector.on('timeout', function (data) {
                console.log('TIMEOUT_COLLECTOR: ' + socketCollector.remoteAddress + ' ' + socketCollector.remotePort);
                socketCollector.end();
                reject("socket timeout");
                releasePort(collectorPort);
            });
            socketCollector.on('error', function (data) {
                console.log('ERROR_COLLECTOR: ' + socketCollector.remoteAddress + ' ' + socketCollector.remotePort);
                socketCollector.end();
                releasePort(collectorPort);
                reject("socket error");
            });
            socketCollector.on('close', function (data) {
                console.log('CLOSED_COLLECTOR: ' + socketCollector.remoteAddress + ' ' + socketCollector.remotePort);
                socketCollector.end();
                releasePort(collectorPort);

                if (result && result !== "") {
                    const remotePort = socketCollector.remotePort;

                    const key = cryptoManager.sha256(collectorPort.toString() + environment.HOSTNAME);
                    const iv = cryptoManager.md5(environment.HOSTNAME + collectorPort.toString());
                    const message = cryptoManager.aes256Decrypt(result.toString(), key, iv);

                    console.log("Reading from SocketCollector" + collectorPort + ":" + remotePort + " -> " + message);
                    resolve(message);
                } else {
                    reject("empty result");
                }
            });
        }).listen(collectorPort);
    });
}

async function releasePort(port) {
    socketsCollectorPool.set(port, {status: "not_used", endTime: Math.floor(new Date().getTime() / 1000)});
}


module.exports = {
    requireFreeCodeCollectorPort,
    openSocketCollectorAndWaitForResult,
    releasePort
};
