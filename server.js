/** Server Dependencies **/
const http = require('http');
const app = require('./app');
const environment = require('./environment')


/** Define the server PORT **/
app.set('port', environment.SERVER_PORT);


/** Create a new HTTP server instance **/
const server = http.createServer(app);


/** Start server and make it listen on a PORT **/
server.listen(environment.SERVER_PORT);


/** Server Callbacks **/
server.on('listening', () => {
    console.log('Server started and listening on port ' + environment.SERVER_PORT);
});
server.on('error', (error) => {
    if (error.syscall !== 'listen') {
        throw error;
    }

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(environment.SERVER_PORT + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(environment.SERVER_PORT + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
});