const express = require('express');


const healthcheckRouter = express.Router();


healthcheckRouter
    .get("/", (req, res) => {
        res.json({
            status: "alive"
        });
    });


module.exports = healthcheckRouter;